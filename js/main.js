jQuery(document).ready(function($) {
	// activate popover hover for bts with class popover-btn
	$('.popover-btn').popover({ trigger: 'hover'})


	// ru локализация для датапикера
	// инициализируем датапикер и обрабатываем ивент при изменении
	// выставляем ему длину в зависимости от длины инпута тк. сайт адаптивный
	$('.datepicker').datepicker({
	    language: 'ru',
	    format: 'dd.mm.yyyy',
    	autoclose:true
	    // если нужен укр. вписать 'ua' и погрзугить вместо 
	    // bootstrap-datepicker.ru.js bootstrap-datepicker.ua.js
	})
	// при изменении датапикера вызиваем функцию
	// что что бы увеличить размер календаря по размера инпута
	.change(dataChanged)
	.on('show', dataChanged);
	
	// очищаем инпуты по клику ВСЕ ДАТЫ
	$('#datepickclear').click(function() {
		$('#datepicker,#datepicker2').val('');
	});


	// заменяет текст для дроп дауна при изменении его
	$(".dropdown-menu li a").click(function(){
	  var selText = $(this).text();
	  $(this).parents('.dropdown.open').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	});

	if (!! $(".chzn-select")) {
	   // element exists...
		$(".chzn-select").chosen();
	}	

	if (!! $('.multiselect')) {
	   // element exists...
		$('.multiselect').multiselect({
			numberDisplayed: 2,
			nonSelectedText: 'любой',
			nSelectedText: ' - выбрано опции',
			allSelectedText: 'Выбрано все опции'
		});


	}


	// форматируем сумму заказа по щаблону
	$('.num_format').number( true, 2, ',', ' ' );



	// обрезать в 40 символов
	$('.text-coment').each(function() {
		// находим все елементы с класом text-coment
		var tleng, cuttext, count=60, title, coment;
		// tleng длина текста  в символах
		// count максимальная длина текста в символах
		// cuttext обрезаный текст в таблице
		// для каждего из них детей с класом имя и комент
		title = $(this).children(".name").text();
		coment = $(this).children(".coment").text();
		// получаем текст из елементов детей
		tleng = coment.length;
		// узнаем длину тектста коментария
		// проверяем если она больше чем задана в count=60 оберзаем
		if(tleng > count ){

			cuttext = coment.substring(0,count);
			cuttext = $.trim(cuttext);

			$(this).addClass("popover-btn")
			       .data('container', "body").attr('data-container', "body")
				   .data('toggle', "popover").attr('data-toggle', "popover")
				   .data('placement', "left").attr('data-placement', "left")
				   .data('original-title', title).attr('data-original-title', title)
				   .data('content', coment).attr('data-content', coment)
				   .text(cuttext+'...');
		}
		$('.popover-btn').popover({ trigger: 'hover'})

	});

	// при ховере изменяем позицию сверху тултайпа
	// что бы постапвить его влево
	$('.popover-btn').hover(function() { 

		var b = $('.popover-btn').height()
		var p = $(".popover.in").position().top;
		var h = $(".popover.in").height();
		h = h /2;
		p = p + h - b;
		$(".popover.in").css("top",p);
	});

	
	// обрабатываем нажанте на отключонную кнопку
	$('li.disabled > a').click(function() { return false; });


	// по нажатию кнопки удлить запись деалем это :)
	$('.del > .fa').click(function() {
		var deleteobj = $(this).parents('tr');
		var numb = $(this).parents('tr').children(".numb").text();
		var date = $(this).parents('tr').children(".date").text();
		deleteaction(deleteobj,numb,date);
	});

	// по нажатив в модальном окне Удалить закрываем модальное окно и удаляем 
	$('.del-modal .btn-success').click(function() {
		$('.del-modal').modal('show');
		$('.del-modal').modal('hide');
		$(this).parents('tr').remove();
	});


	// при нажатии проверяем есть ли у линии тег data-href
	// и перемещаемся по линку
	$('td.numeric').click(function () {
	    if($(this).parents('tr').data('href') !== undefined){
	        document.location = $(this).parents('tr').data('href');
	    }
	});
	
	curentDataInInput();

	$('.table').DataTable( {
		"dom": '<"top clearfix"ilp>rt<"bottom clearfix"ilp>',
		"language": {
			"url": "http://dev.applemint.eu/artem/agromat/js/locales/dataTables.ru.json"
        },
        "lengthMenu": [[20, 50, 100], [20, 50, 100]],

        "order": [[ 1, "asc" ]],
        "columns": [
			{ "orderable": false },
			null,
			{ "orderable": false },
			null,
			{ "orderable": false },
			{ "orderable": false },
			{ "orderable": false }
		]
        
    } );

	var table = $('.table').DataTable();
 	// #column3_search is a <input type="text"> element
	$('#column_search').on( 'keyup', function () {
	    table
	        .columns( 0 )
	        .search( this.value )
	        .draw();
	} );
});

	function dataChanged(ev) {
		var datainput = '#datepicker,#datepicker2';

		var datainpuwidth = $(datainput).width() + 26;
		// 26 is a pixels for padding
		$('.datepicker-dropdown').css('width', datainpuwidth);
	}

	function curentDataInInput(ev){
		var datainputFirst = '#datepicker';
		var datainputCurr = '#datepicker2';
		// переменые инпуты
		var currentYear = (new Date).getFullYear();
		var currentMonth = (new Date).getMonth() + 1;
		var currentDay = (new Date).getDate();
		// получаем дату (день месяц год)
		// если нужно записать value в инпут раскоментироваь строку 108 и 111
		$(datainputFirst).val("1."+currentMonth+"."+currentYear);
		// $(datainputFirst).attr("placeholder", "1."+currentMonth+"."+currentYear);
		
		$(datainputCurr).val(currentDay+"."+currentMonth+"."+currentYear);
		// $(datainputCurr).attr("placeholder", currentDay+"."+currentMonth+"."+currentYear);

	}

	function deleteaction(deleteobj,numb,date){
		$('.del-modal').modal('show');
		$('.del-modal .numb').text(numb);
		$('.del-modal .date').text(date);
		// Hide modal if "Okay" is pressed
		$('.del-modal .btn-success').click(function() {
			$('.del-modal').modal('hide');
			$(deleteobj).remove();
		});
	}